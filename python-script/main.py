import connection as con
import requests
import json

api = "https://api.open-meteo.com/v1/forecast?latitude=12.9716&longitude=77.5946&daily=temperature_2m_max,temperature_2m_min,sunrise,sunset,rain_sum&timezone=Asia%2FSingapore&forecast_days=1"


def main():
    print('starting connection')
    # start coonection
    cursor  = con.connection.cursor()



    # get daily weather data
    try:
        res = requests.get(api)
        data_weather = json.loads(res.text)['daily']
        print(data_weather)
        # data_weather =  {'time': ['2024-01-24'], 'temperature_2m_max': [29.2], 'temperature_2m_min': [17.5], 'sunrise': ['2024-01-24T09:16'], 'sunset': ['2024-01-24T20:46'], 'rain_sum': [0.0]}
        dates = data_weather['time']
        temp_max = data_weather['temperature_2m_max']
        temp_min = data_weather['temperature_2m_min']
        sunrise = data_weather['sunrise']
        sunset = data_weather['sunrise']
        rain = data_weather['rain_sum']

        # now push to sql 
       
    
    except:
        print("failed to fetch")
        dates = None
        temp_max = None
        temp_min = None
        sunrise = None
        sunset = None
        rain = None

        return 
    
    #adding to the database
    try:
        print("adding data")
        if(sunrise[0]):
            sunrise = sunrise[0][11:]
        # print(sunrise)
        if(sunset[0]):
            sunset = sunset[0][11:]
        temp_max = temp_max[0]
        temp_min = temp_min[0]
        rain = rain[0]
        print(dates,temp_max,temp_min,sunrise,sunset,rain)
        sqlsntx = 'INSERT INTO weathertable(w_date,temp_max,temp_min,sunrise,sunset,rain) values(%s,%s,%s,%s,%s,%s)'
        cursor.execute(sqlsntx,(dates[0],temp_max,temp_min,sunrise,sunset,rain))
        # cursor.close()
        con.connection.commit()
        con.connection.close()
        print("Query Successfully Completed")
    except Exception as e:
        print("********ERROR FAILED",e)
        return
    return
def printall():
    cursor = con.connection.cursor()
    cursor.execute('Select * from weathertable')
    w = cursor.fetchall()
    for x in w:
        print(w,end='\n')
if __name__=="__main__":

    main()
    # printall()
