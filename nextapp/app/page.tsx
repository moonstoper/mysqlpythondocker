// import Image from "next/image";
'use client'
import axios from "axios"
import { useEffect, useState } from "react";
import Navbar from "./components/Navbar"
// import { Providers } from "./providers";
import { Button, FileButton, MantineProvider, Table } from '@mantine/core';
import { LineChart, AreaChart } from '@mantine/charts';
import { Line } from "recharts";
import { url } from "inspector";
import Link from "next/link";
// import {Chart}

type weatherdata = [] | null
let dss = [
  {
    date: 'Mar 22',
    Apples: 2890,
    Oranges: 2338,
    Tomatoes: 2452,
  },
  {
    date: 'Mar 23',
    Apples: 2756,
    Oranges: 2103,
    Tomatoes: 2402,
  },
  {
    date: 'Mar 24',
    Apples: 3322,
    Oranges: 986,
    Tomatoes: 1821,
  },
  {
    date: 'Mar 25',
    Apples: 3470,
    Oranges: 2108,
    Tomatoes: 2809,
  },
  {
    date: 'Mar 26',
    Apples: 3129,
    Oranges: 1726,
    Tomatoes: 2290,
  },
]
export default function Home() {
  const [formapi, setfromapi] = useState<string>("alphabet")
  const [weatherdata, setweatherdata] = useState<weatherdata>(null)

  function indianStandardTIme(t: string): string {
    var hr = Number(t.slice(0, 2))
    var min = Number((Number(t.slice(3, 5)) / 60).toFixed(2))
    var newdTime = (hr + min) - 2.50
    var smin: any = 60 * (newdTime % 1)
    smin = Math.round(smin.toFixed(2))
    var shr = newdTime.toString()
    if (shr[1] == '.') {
      return '0' + shr[0] + ':' + smin
    } else {
      return shr.slice(0, 2) + ':' + smin
    }
  }

  async function callapi() {
    let a = await axios.post("/api/fetchall", { nice: "butts" })
    if (!a.data.err) {
      console.log(a)
      a.data.res.forEach((dt: any, index: any) => {
        a.data.res[index] = {
          ...dt,
          temp_max: dt.temp_max.toFixed(1),
          temp_min: dt.temp_min.toFixed(1),
          sunrise: indianStandardTIme(dt.sunrise),
          sunset: indianStandardTIme(dt.sunset),
          rain: dt.rain
        }
      })
      setweatherdata(a.data.res)
      // console.log(weatherdata)
    }
    console.log(weatherdata)
  }


  useEffect(() => {
    callapi()
  }, [])
  return (
    <MantineProvider>


      <div>

        <Navbar></Navbar>
        {/* <table>
          <thead>
            <tr>

              <th>Index</th>
              <th>Date</th>
              <th>MAX Temperature in C</th>
              <th>MIN Temperature in C</th>
              <th>Sunrise</th>
              <th>Sunset</th>
              <th>Rain in mm</th>
            </tr>
          </thead>
          <tbody>
            {
              weatherdata != null ?
                weatherdata.map((data: any, index) => {
                  return (
                    <tr key={index}>
                      <td>{index + 1}</td>
                      <td>{data.w_date}</td>
                      <td>{data.temp_max}</td>
                      <td>{data.temp_min}</td>
                      <td>{data.sunrise}</td>
                      <td>{data.sunset}</td>
                      <td>{data.rain}</td>
                    </tr>
                  )
                }) : null
            }
          </tbody>
        </table> */}
        <Table>
          <Table.Thead>
            <Table.Tr>
              <Table.Th>Index</Table.Th>
              <Table.Th>Date</Table.Th>
              <Table.Th>temp_max</Table.Th>
              <Table.Th>temp_min</Table.Th>
              <Table.Th>Sunrise</Table.Th>
              <Table.Th>Sunset</Table.Th>
              <Table.Th>Rain in mm</Table.Th>
            </Table.Tr>
          </Table.Thead>
          <Table.Tbody>
            {
              weatherdata != null ?
                weatherdata.map((data: any, index) => {
                  return (
                    <Table.Tr key={index}>
                      <Table.Td>{index + 1}</Table.Td>
                      <Table.Td>{data.w_date}</Table.Td>
                      <Table.Td>{data.temp_max}</Table.Td>
                      <Table.Td>{data.temp_min}</Table.Td>
                      <Table.Td>{data.sunrise}</Table.Td>
                      <Table.Td>{data.sunset}</Table.Td>
                      <Table.Td>{data.rain}</Table.Td>
                    </Table.Tr>
                  )
                }) : null
            }
          </Table.Tbody>
        </Table>
        {
          // weatherdata &&
        //  <Link href={"/charts"} >Chart</Link>

        }


      </div>
    </MantineProvider>
  );
}
