import React from "react";
import Image  from "next/image";
// import {AcmeLogo} from "./AcmeLogo.jsx";
import Thermo from "../asset/termo.svg"
export default function App() {

    return (
        <nav className="flex flex-row h-20 bg-black">
            <div className='flex flex-row justify-start  gap-1 drop-shadow-lg items-center bg-opacity-10'>
                {/* <img src={Thermo} className='w-5 ' ></img> */}
                <Image src={Thermo} alt="thermometer" className="w-5"></Image>
                <div className='flex flex-col justify-start text-xs text-white font-bold drop-shadow-lg'>
                    <div>
                        THE
                    </div>
                    <div>
                        CURRENT
                    </div>
                    <div>
                        WEATHER
                    </div>
                </div>
            </div>

        </nav>
    )

}
