USE db;

CREATE TABLE weathertable(w_date date PRIMARY KEY,temp_max float(1),temp_min float(1), sunrise varchar(50), sunset varchar(50), rain float(2) );

load data infile '/var/lib/mysql-files/weather.csv' into table weathertable fields terminated by ',' lines terminated by '\n'; 